# base image
FROM node:13.6.0

# set working directory
WORKDIR /usr/src/app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package*.json ./
RUN npm install
RUN npm install -g @angular/cli

# Bundle app source code
COPY . /usr/src/app

EXPOSE 4200

# start app

CMD ["ng", "serve", "--proxy-config", "proxy.config.json"]
