#!/bin/sh

docker build -t nibupac/factures-frontend https://framagit.org/factures/angular-frontend
docker build -t nibupac/factures-backend https://framagit.org/factures/node-backend
