export class Service {
    id: number;
    label: string;
    startingDate: string;
    endingDate: string;
    dailyTariff: number;

    constructor(label, startingDate, endingDate: string, dailyTariff: number) {
        this.label = label;
        this.startingDate = startingDate;
        this.endingDate = endingDate;
        this.dailyTariff = dailyTariff;
    }
}

export class Bill {
    number: number;
    date: string;
    billingAddressL1: string;
    billingAddressL2: string;
    billingAddressL3: string;
    PDFGenerated: boolean;
    services: Service[];

    constructor(num: number, date, billingAddressL1, billingAddressL2,
                billingAddressL3 : string) {
        this.number = num;
        this.date = date;
        this.billingAddressL1 = billingAddressL1;
        this.billingAddressL2 = billingAddressL2;
        this.billingAddressL3 = billingAddressL3;
    }
}
