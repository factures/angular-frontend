import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BillService } from '../bill.service';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    brief: any;

    constructor(private http: HttpClient, private billService: BillService,
                private alertService: AlertService) { }

    ngOnInit() {
        this.loadBills();
    }

    loadBills() {
        this.http.get('/api/bill/brief').toPromise()
            .then(brief => {
                console.warn(brief)
                this.brief = brief;
            });
    }

    deleteBill(num: number) {
        this.billService.deleteBill(num).toPromise()
            .then(res => {
                this.alertService.success('Bill #' + num + ' has been deleted');
                this.loadBills();
            }).catch(error => {
                this.alertService.failure('Bill #' + num + ' has not been deleted');
            });
    }

    generatePDF(num: number) {
        this.http.get<object>('/api/bill/' + num + '/pdf/generate').toPromise()
            .then(res => {
                console.warn('Answer for PDF generation: ', res);

                if (res.hasOwnProperty('message')) {
                    this.alertService.success(res['message']);
                }
            })
            .catch(error => {
                this.alertService.failure('<strong>Failure</strong> PDF generation failed');
            });
    }
}
