import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Bill, Service } from '../bill';
import { BillService } from '../bill.service';
import { AlertService } from '../alert.service';

@Component({
    selector: 'app-edit-bill',
    templateUrl: './edit-bill.component.html',
    styleUrls: ['./edit-bill.component.scss']
})
export class EditBillComponent implements OnInit {
    editBillForm = this.formBuilder.group({
        billDate: [''],
        billingAddressL1: [''],
        billingAddressL2: [''],
        billingAddressL3: ['']
    });

    addServiceForm = this.formBuilder.group({
        serviceLabel: [''],
        serviceStartingDate: [Date.now()],
        serviceEndingDate: [Date.now()],
        serviceDailyTariff: ['']
    });

    bill: Bill;
    services: Service[];

    constructor(private http: HttpClient, private formBuilder: FormBuilder,
                private alertService: AlertService,
                private billService: BillService,
                private route: ActivatedRoute,
                private router: Router) { }

    ngOnInit() {
        this.route.paramMap.pipe(switchMap(params => {
            return this.billService.getBill(+params.get('num'));
        })).subscribe(bill => {
            this.bill = bill;
            this.services = bill.services;
            this.fillBillInfo(bill);
        });
    }

    submitBillForm() {
        this.http.post<Bill>('/api/bill/' + this.bill.number, this.getBill()).toPromise()
            .then(() => {
                this.alertService.success('Bill sending success');
            }, (error) => {
                this.alertService.failure('Bill sending failure');
            });
    }

    private fillBillInfo(bill: Bill) {
        this.editBillForm.setValue({
            billDate: new Date(bill.date),
            billingAddressL1: bill.billingAddressL1,
            billingAddressL2: bill.billingAddressL2,
            billingAddressL3: bill.billingAddressL3
        });
    }

    private getBill() {
        let bill = new Bill(this.bill.number,
                            this.editBillForm.controls['billDate'].value,
                            this.editBillForm.controls['billingAddressL1'].value,
                            this.editBillForm.controls['billingAddressL2'].value,
                            this.editBillForm.controls['billingAddressL3'].value);
        return bill;
    }

    private getNewService() {
        return {
            label: this.addServiceForm.controls['serviceLabel'].value,
            startingDate: this.addServiceForm.controls['serviceStartingDate'].value,
            endingDate: this.addServiceForm.controls['serviceEndingDate'].value,
            dailyTariff: this.addServiceForm.controls['serviceDailyTariff'].value
        };
    }

    updateBill() {
        return this.billService.getBill(this.bill.number).toPromise()
            .then(bill => {
                this.bill = bill;
                this.services = bill.services;
                this.fillBillInfo(bill);
                return bill;
            });
    }

    submitServiceForm() {
        this.http.post('/api/bill/' + this.bill.number + '/service/new', this.getNewService()).toPromise()
            .then(() => {
                this.alertService.success('Service adding success');
                return this.updateBill();
            }, (error) => {
                this.alertService.failure('Service adding failure: ' + error);
            });
    }

    deleteService(id) {
        this.http.post('/api/bill/' + this.bill.number + '/service/delete',
                       { 'id': id }).toPromise()
            .then(res => {
                this.alertService.success('Service has been deleted');
                return this.billService.getBill(this.bill.number).toPromise();
            }).then(bill => {
                return this.updateBill();
            }).catch((error) => {
                this.alertService.failure('<strong>Error</strong> Service has not been deleted');
            });
    }
}
