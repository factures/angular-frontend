import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Bill } from '../bill';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-create-bill',
  templateUrl: './create-bill.component.html',
  styleUrls: ['./create-bill.component.scss']
})
export class CreateBillComponent implements OnInit {
    createBillForm = this.formBuilder.group({
        billNumber: [''],
        billDate: [''],
        billingAddressL1: [''],
        billingAddressL2: [''],
        billingAddressL3: ['']
    });

    constructor(private http: HttpClient, private formBuilder: FormBuilder,
                private alertService: AlertService, private router: Router) { }

    ngOnInit() {
    }

    submitBillForm() {
        this.http.post<Bill>('/api/bill/new', this.getBill()).toPromise()
            .then(() => {
                this.alertService.success('Bill sending success');
                this.router.navigate(['bill']);
            }, (error) => {
                this.alertService.failure('Bill sending failure');
            });
    }

    getBill() {
        return new Bill(this.createBillForm.controls['billNumber'].value,
                        this.createBillForm.controls['billDate'].value,
                        this.createBillForm.controls['billingAddressL1'].value,
                        this.createBillForm.controls['billingAddressL2'].value,
                        this.createBillForm.controls['billingAddressL3'].value);
    }
}
