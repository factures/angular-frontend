import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from '../alert.service';

@Component({
    selector: 'app-alert-zone',
    templateUrl: './alert-zone.component.html',
    styleUrls: ['./alert-zone.component.scss']
})
export class AlertZoneComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    alerts: object[] = [];

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.subscription = this.alertService.getAlerts()
            .subscribe(message => {
                switch (message && message.type) {
                    case 'success':
                        message.cssClass = 'alert alert-success';
                        break;
                    case 'failure':
                        message.cssClass = 'alert alert-danger';
                        break;
                }

                // Bug here

                if (this.alerts.length >= 3) {
                    this.alerts.shift();
                }

                this.alerts.push(message);
                setTimeout(() => this.alerts.shift(), 5000);
            });
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
