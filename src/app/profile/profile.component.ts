import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { User } from '../user';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    profileForm = this.formBuilder.group({
            companyName: [''],
            companyAddress: [''],
            companyNumber: [''],
            phoneNumber: ['']
        });

    constructor(private http: HttpClient, private formBuilder: FormBuilder,
                private alertService: AlertService) { }

    ngOnInit() {
        this.getProfile();
    }

    onSubmit() {
        this.http.post<User>('/api/profile', this.getUser()).toPromise()
            .then(() => {
                this.alertService.success('Profile sending success');
            }, (error) => {
                this.alertService.failure('Profile sending failure');
            });
    }

    private getProfile() {
        this.http.get<User>('/api/profile').toPromise()
            .then(user => {
                if (user) {
                    this.profileForm.setValue({
                        companyName: user.companyName,
                        companyAddress: user.companyAddress,
                        companyNumber: user.companyNumber,
                        phoneNumber: user.phoneNumber
                    });
                }
            });
    }

    private getUser(): User {
        return new User(this.profileForm.controls['companyName'].value,
                        this.profileForm.controls['companyAddress'].value,
                        this.profileForm.controls['companyNumber'].value,
                        this.profileForm.controls['phoneNumber'].value);
    }
}
