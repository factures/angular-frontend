import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Bill } from './bill';

@Injectable({
    providedIn: 'root'
})
export class BillService {

    constructor(private http: HttpClient) { }

    getBill(num: number): Observable<Bill> {
        return this.http.get<Bill>('/api/bill/' + num);
    }

    deleteBill(num: number): Observable<object> {
        return this.http.get('/api/bill/' + num + '/delete');
    }
}
